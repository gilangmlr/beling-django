from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Gilang Gumilar'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1996, 1, 10)
npm = 1506737792


def index(request):
    response = {'name': mhs_name, 'age': calculate_age(
        birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
